package no.uib.inf101.terminal;

public class CmdPwd implements Command{
    private Context context;
    @Override
    public String run(String[] args) {
        return this.context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName() {
        return "pwd";        
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
     }
    
    @Override
    public String getManual() {
        String output = "Denne kommandoen printer ut den mappen du nå arbeider i";
        return output;
    }
}
