package no.uib.inf101.terminal;

import java.io.File;

public class CmdLs implements Command{
    //instansvariabel for konteksten i terminalen 
    private Context context; 
    @Override
    public String run(String[] args) {
        File cwd = this.context.getCwd();
        String s = "";
        for (File file : cwd.listFiles()) {
        s += file.getName();
        s += " ";
        }
        return s;

    }

    @Override
    public String getName() {
        return "ls";        
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
     }

     @Override
    public String getManual() {
        String output = "Skriv inn denne kommandoen for å få en liste på alle filene i mappen du er i";
        return output;
     }
    

}
