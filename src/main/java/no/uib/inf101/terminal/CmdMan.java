package no.uib.inf101.terminal;

import java.util.HashMap;

public class CmdMan implements Command{
    HashMap<String, Command> commandContext;

    @Override
    public String run(String[] args) {
        String name = "";
        for (String word : args) {
            name+= word + "";
        }
        Command cmd = commandContext.get(name);
        String output = cmd.getManual();
        return output;
    }

    @Override
    public String getName() {
        return "man";        
    }


     @Override
    public String getManual() {
        String output = "Hvis du får denne, så vet du nok fra før...";
        return output;
     }
    
    @Override
    public void setCommandContext(HashMap<String, Command> commandContext) {
        this.commandContext = commandContext;
    }
}
