package no.uib.inf101.terminal;

public class CmdCd implements Command{
    private Context context;
    @Override
    public String run(String[] args) {
        if (args.length == 0) {
            this.context.goToHome();
            return "";
          } else if (args.length > 1) {
            return "cd: too many arguments";
          }
          String path = args[0];
          if (this.context.goToPath(path)) {
            return "";
          } else {
            return "cd: no such file or directory: " + path;
          }
    }

    @Override
    public String getName() {
        return "cd";        
    }
    @Override
    public void setContext(Context context) {
        this.context = context;
     }
    
    @Override
    public String getManual() {
        String output = "Denne kommandoen kan brukes til å bytte hvilken mappe du arbeider i.\n"
                      + "Du kan skrive cd 'navn' for å bytte til en mappe som er i den nåværende mappen,\n"
                      + "eller skrive cd .. for å gå en tilbake.";
        return output;
    }
    
}

