package no.uib.inf101.terminal;

import java.util.HashMap;

public interface Command {
    //Metodesignatur for å run
    String run(String[] args);

    //Metodesignatur for å finne navnet på kommandoen
    String getName(); 
    
    default void setContext(Context context) { /* do nothing */ };

    default void setCommandContext(HashMap<String, Command> commandContext) {/* do nothing */};

    //Skriv inn kommandoen man for å få en manual på hvordan du kan bruke terminalen 
    //og hva de forskjellige kommandoene gjør 
    String getManual();

}
