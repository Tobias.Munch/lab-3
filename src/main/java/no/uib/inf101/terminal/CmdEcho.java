package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        String result = "";
        for (String word : args) {
            result+= word + " ";
        }
        return result;
    }

    @Override
    public String getName() {
        return "echo";        
    }

    @Override
    public String getManual() {
        String output = "Alt du skriver etter denne kommandoen blir gjenntatt";
        return output;
    }
    
}
